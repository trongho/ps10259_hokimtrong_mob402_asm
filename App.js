//file server
var http=require('http');
//route
var express = require('express');
var bodyParser=require('body-parser');
var app = express();
var urlencodeParser=bodyParser.urlencoded({ extended: false });

app.use(express.static('public'));



var server=http.createServer(app)
server.listen(3000);

var user={};//biến toàn cục


//trang login
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/login.html');
});


//trang chủ
app.get('/index', function (req, res) {
    if(!user.username)
    {
        //res.sendFile(__dirname + '/login.html');
        res.writeHead(301,{"Location":"http://"+req.headers['host']+'/'});
        res.end();
    }
    res.sendFile(__dirname + '/index.html');
});


//xử lý đăng nhập
app.post('/loginProcess',urlencodeParser,function(req,res){
    user={
        username:req.body.email,
        password:req.body.password
    }
    if(user.username=="tronghkps10259@fpt.edu.vn"&&user.password=="123"){
        res.sendFile(__dirname + '/index.html');
    }else{
        res.sendFile(__dirname + '/login.html');
    }
});

//chuyển trang hồ sơ cá nhân
app.get('/ho_so_ca_nhan', function (req, res) {
    res.sendFile(__dirname + '/ho_so_ca_nhan.html');
});

//chuyển trang thể loại
app.get('/theloai', function (req, res) {
    res.sendFile(__dirname + '/theloai.html');
});
